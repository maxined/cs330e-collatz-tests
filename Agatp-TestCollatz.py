#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        
    def test_read_2(self):
        s = "15 20\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  15)
        self.assertEqual(j, 20)
    
    def test_read_3(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 200)
    
    def test_read_4(self):
        s = "12 18\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  12)
        self.assertEqual(j, 18)
    
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
        
    def test_eval_5(self):
        v =  collatz_eval(5000,6000)
        self.assertEqual(v,236)
        
    def test_eval_6(self):
        v =  collatz_eval(900000,999999)
        self.assertEqual(v,507)
        
    def test_eval_7(self):
        v =  collatz_eval(5123,69882)
        self.assertEqual(v,340)
        
    def test_eval_8(self):
        v =  collatz_eval(3333,99999)
        self.assertEqual(v,351)
        
    def test_eval_9(self):
        v =  collatz_eval(1,1)
        self.assertEqual(v,1)

    def test_eval_10(self):
        v =  collatz_eval(999920,999999)
        self.assertEqual(v,259)
    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
   
    def test_print2(self):
        w =  StringIO()
        collatz_print(w,900000,999999,507)
        self.assertEqual(w.getvalue(), "900000 999999 507\n")
    
    def test_print3(self):
        w =  StringIO()
        collatz_print(w,1,1,1)
        self.assertEqual(w.getvalue(), "1 1 1\n")
   
    def test_print4(self):
        w =  StringIO()
        collatz_print(w,201, 210,89)
        self.assertEqual(w.getvalue(), "201 210 89\n")
        
    def test_print5(self):
        w =  StringIO()
        collatz_print(w,0,0,0)
        self.assertEqual(w.getvalue(), "0 0 0\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    
    def test_solve2(self):
        r = StringIO("1 50\n300 450\n15 20\n1 2\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 50 112\n300 450 144\n15 20 21\n1 2 2\n")
    
    def test_solve3(self):
        r = StringIO("1000 1200\n333 666\n2000 4000\n123 432\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1000 1200 182\n333 666 145\n2000 4000 238\n123 432 144\n")
    
    def test_solve4(self):
        r = StringIO("1 1000\n12 13\n50 60\n100 101\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1000 179\n12 13 10\n50 60 113\n100 101 26\n")

# ----age
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
